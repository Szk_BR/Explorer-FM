# Explorer FM for the ZX Spectrum

### Yamaha YMF288 (SSG+OPN3) upgrade over TSFM's dual YM2203 chips

![1st Protoboard](./Pictures/Protoboard1.JPG)

The Explorer FM aims to offer the best sound generation experience to a wide range of ZX Spectrum compatible systems with a plug-in solution, overcoming previous solutions' shortcomings such as the Turbo Sound FM's need to hack into the original PSG istalled in some systems, or the lack of room for a FM synth core in the amazing ZX Next's FPGA.

Systems based on the ZX Spectrum 128/+2/+3 will still require a modification to disable its' internal PSG chip in order to avoid conflict with the ExFM, but otherwise any Speccy compatible system including 16K/48K models with a standard ZX Spectrum expansion bus can take the ExFM and enjoy all the features offered by Yamaha's latest chip from the OPN FM family:

- **SSG**: compatible with YM2203 and YM2149 (I/O ports deleted), 3-channels, mixed to stereo digital output;
- **FM**: OPN3 class, 4-operators, 6-channels (twice the YM2203), mixed to stereo digital output;
- **Rythm**: 6-instruments from embedded ADPCM sampled data, 6-channels, mixed to stereo digital output;

### Software for the YMF288

Regarding the SSG feature, the ExFM is expected to work exactly as vintage Simple-AY, Melodik, Explorer or other devices that provided a PSG chip (GI's AY-3-8912 or similar) when plugged into any ZX Spectrum's expansion bus. Games and demos with soundtracks intended for the ZX Spectrum 128's PSG (including many that ran on 48K models) are expected to play exactly the same way from the single, 3-channel SSG included in the YMF288.

The FM feature in the single YMF288 is expected to work exactly the same as the dual YM2203's in the original TSFM developed by NedoPC Team. Games, demos, trackers and players created in the last 15 years for the TSFM inserted in a Pentagon and other East European Speccy-based machines are expected to work also with the ExFM, provided that they fit in your target ZX Spectrum's RAM and they don't rely exclusively on the TRD (Beta Disk) filesystem, allowing them to run from the file/storage system of your choice.

Now the Rhythm feature in the YMF288 will be a complete novelty for the ZX Spectrum, requiring new software to be developed to take advantage of its' potential, well known in foreign systems such as the Japanese NEC PC88 & PC98.

In short: comparing the ExFM to the TSFM, the SSG is the only ExFM feature that's "inferior". The FM feature should perform equally well, and the Rhythm feature opens a whole new frontier. Hopefully the ExFM will stimulate a new wave of music development to be enjoyed by a larger crowd of ZX Spectrum users worldwide.

### Acknowledgements

Special thanks to NedoPC Team's developers CHRV, lvd, ALCO, Shiru for their amazing creation and documentation; to Velesoft, zOOm and other developers who publish valuable technical information to the Speccy community around the world; and a very special shout to my old friend JBS who materialized the first Explorer interface for the Brazilian TK-90X/95 community.

### Development Diary

- [x] **2021/Jul/2** = NedoPC's TSFM RevC Description and Programming docs translated from Russian (automated translation, far from perfect): Rev1 done.
- [x] **2023/Feb/7** = Yamaha YMF288 Datasheet OCR'ed and re-edited in Japanese for future translation to English: Rev1 done.
- [x] **2023/Feb/14** = Draft Schematic: Rev0.3 done.
- [x] **2023/Apr/17** = 1st Protoboard finished, pending final check and trial tests on actual ZX Next Dev Board.

### Next Steps

- [ ] Translation of the Yamaha YMF288 Datasheet to English
- [ ] Initial protoboard trials: on my next vacation window...